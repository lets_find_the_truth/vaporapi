//
//  User.swift
//  App
//
//  Created by Kyryl Nevedrov on 26/02/2019.
//

import Authentication
import Vapor
import FluentSQLite

final class User: SQLiteModel{
    var id: Int?
    var name: String
    var email: String
    var passwordHash: String
    
    init(name: String, email: String, passwordHash: String, id: Int? = nil) {
        self.name = name
        self.email = email
        self.passwordHash = passwordHash
        self.id = id
    }
}

extension User: PasswordAuthenticatable {
    static var usernameKey: WritableKeyPath<User, String> {
        return \.email
    }
    
    static var passwordKey: WritableKeyPath<User, String> {
        return \.passwordHash
    }
}

extension User: TokenAuthenticatable {
    typealias TokenType = UserToken
}

extension User: Migration {
    static func prepare(on conn: SQLiteConnection) -> Future<Void> {
        return SQLiteDatabase.create(User.self, on: conn) { builder in
            builder.field(for: \.id, isIdentifier: true)
            builder.field(for: \.name)
            builder.field(for: \.email)
            builder.field(for: \.passwordHash)
            builder.unique(on: \.email)
        }
    }
}
extension User: Content { }
extension User: Parameter { }
