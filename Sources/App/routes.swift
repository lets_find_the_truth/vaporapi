import Vapor
import Crypto

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    let userController = UserController()
    // signUp route
    router.post("users", use: userController.create)
    // signIn route
    let basic = router.grouped(User.basicAuthMiddleware(using: BCryptDigest()))
    basic.post("login", use: userController.login)
    // todo routes
    let todoController = TodoController()
    let bearer = router.grouped(User.tokenAuthMiddleware())
    bearer.get("todos", use: todoController.index)
    bearer.post("todos", use: todoController.create)
    bearer.delete("todos", Todo.parameter, use: todoController.delete)
}
