//
//  UserController.swift
//  App
//
//  Created by Kyryl Nevedrov on 26/02/2019.
//

import Vapor
import FluentSQLite
import Crypto

final class UserController{
    func create(_ req: Request) throws -> Future<UserResponse> {
        // decode request content
        return try req.content.decode(CreateUserRequest.self).flatMap { user -> Future<User> in
            // verify that passwords match
            guard user.password == user.verifyPassword else {
                throw Abort(.badRequest, reason: "Password and verification must match.")
            }
            
            // hash user's password using BCrypt
            let hash = try BCrypt.hash(user.password)
            // save new user
            return User(name: user.name, email: user.email, passwordHash: hash)
                .save(on: req)
            }.map { user in
                // map to public user response (omits password hash)
                return try UserResponse(id: user.requireID(), name: user.name, email: user.email)
        }
    }
    
    func login(_ req: Request) throws -> Future<UserToken> {
        let user  = try req.requireAuthenticated(User.self)
        let token = try UserToken.create(userID: user.requireID())
        return token.save(on: req)
    }
}

/// Data required to create a user.
struct CreateUserRequest: Content {
    /// User's full name.
    var name: String
    
    /// User's email address.
    var email: String
    
    /// User's desired password.
    var password: String
    
    /// User's password repeated to ensure they typed it correctly.
    var verifyPassword: String
}

/// Public representation of user data.
struct UserResponse: Content {
    /// User's unique identifier.
    /// Not optional since we only return users that exist in the DB.
    var id: Int
    
    /// User's full name.
    var name: String
    
    /// User's email address.
    var email: String
}
