import Vapor
import FluentSQLite

/// Controls basic CRUD operations on `Todo`s.
final class TodoController {
    /// Returns a list of all `Todo`s.
    func index(_ req: Request) throws -> Future<[Todo]> {
        let user = try req.requireAuthenticated(User.self)
        return try Todo.query(on: req)
            .filter(\.userID == user.requireID()).all()
    }

    /// Saves a decoded `Todo` to the database.
    func create(_ req: Request) throws -> Future<Todo> {
        let user = try req.requireAuthenticated(User.self)
        
        // decode request content
        return try req.content.decode(CreateTodoRequest.self).flatMap { todo in
            // save new todo
            return try Todo(title: todo.title, userID: user.requireID())
                .save(on: req)
        }
    }

    /// Deletes a parameterized `Todo`.
    func delete(_ req: Request) throws -> Future<HTTPStatus> {
        let user = try req.requireAuthenticated(User.self)
        
        // decode request parameter (todos/:id)
        return try req.parameters.next(Todo.self).flatMap { todo -> Future<Void> in
            // ensure the todo being deleted belongs to this user
            guard try todo.userID == user.requireID() else {
                throw Abort(.forbidden)
            }
            
            // delete model
            return todo.delete(on: req)
        }.transform(to: .ok)
    }
}

struct CreateTodoRequest: Content {
    var title: String
}
